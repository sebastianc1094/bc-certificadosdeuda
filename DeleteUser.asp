<html>
<head>
	<meta charset="UTF-8">
    <link rel="icon" href="img/icon.ico">
	<meta http-equiv="refresh" content="3; url=Index.asp" />
    <link rel="stylesheet" type="text/css" href="css/loader.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <script type="text/javascript" src="js/jquery-2.1.3.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script> 
    <title>Eliminación de usuario</title>
</head>
<body>
    <%
        perfil = "0"
        user = lcase(request.serverVariables("AUTH_USER"))

		identificacion = request.form("identificacion")

		Set cnx = Server.CreateObject("ADODB.Connection")
		cnx.Open = "Provider=Microsoft.Jet.OLEDB.4.0; DATA SOURCE=" & Server.mappath("./certs/DB.mdb")

        Set rs = server.createObject("ADODB.RecordSet")
        Set rsUser = server.createObject("ADODB.RecordSet")
		rs.Open "DELETE * FROM tbl_users WHERE Documento="& identificacion &"", cnx
        rsUser.Open "SELECT Perfil FROM tbl_users WHERE UsuarioBC='"& user &"'", cnx

		if rsUser.EOF = false then
            perfil = rsUser.fields(0).value          
		end if
    %>

    <% if perfil = "Administrativo" then %>
        <div style="margin:100px">
            <img src="img/logo.png" style="width:315px; height:80px">
        </div>
        <div style="margin-left:100px">
            <h3>Eliminación exitosa</h3>
            <h4>EL USUARIO HA SIDO ELIMINADO CON ÉXITO.</h4>
            <h4>POR FAVOR ESPERE MIENTRAS SE LE REDIRECCIONA AL PORTAL PRINCIPAL</h4>
            <div class="loader">
            </div>
            <h4 style="display:none"><%=rs%></h4>
        </div>
    <% end if %>
    <% if perfil = "0" then %>
        <script language="Javascript">
            $(document).ready(function(){
                $("#showMod").modal("show");
            });
        </script>
        <div class="modal fade" id="showMod" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <img src="img/logo.png" style="float:left; width:140px; height:35px">
                    </div>
                    <div class="modal-body">
                        <h3 class="modal-title">Revise sus credenciales de acceso<span class="glyphicon glyphicon-remove" style="margin-left:10px"></span></h3>
                        <h4>No dispone de los permisos administrativos para ingresar al portal.</h4>
                        <h4>Por favor contacte a su auxiliar administrativo para solicitar el acceso.</h4>
                    </div>
                    <div class="modal-footer">
                        <a style="text-decoration:none; font-size:20px; color:black" href="mailto:sebastian.castanomo@upb.edu.co?subject=Solicitud de acceso a plataforma de generación de certificados de deuda&body=DATOS DE INSCRIPCIÓN A PORTAL DE GENERACIÓN DE CERTIFICADOS DE DEUDA %0D%0A %0D%0ANOMBRES Y APELLIDOS COMPLETOS: %0D%0ADOCUMENTO DE IDENTIDAD: %0D%0AAREA: %0D%0AUSUARIO DE RED BANCOLOMBIA: ">Solicitar acceso<span class="glyphicon glyphicon-envelope" style="margin-left:10px"></span></a>
                        <h4 style="display:none"><%=rsUser%></h4>
                    </div>
                </div>
            </div>
        </div>
    <% end if %>
    <%
        rs.Close
        cnx.close
    %>
</body>
</html>