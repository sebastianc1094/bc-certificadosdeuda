//Funciones para mostrar/cerrar contenedores

////////////////////////////////////////////////////////
//LÓGICA PARA CONTENEDORES DE PERFIL COMERCIAL
//Contenedores por tipo de fecha
function showFechaSaldosContainer() {
    fechaSaldosContainer.style.display = '';
    fechaMovsContainer.style.display = 'none';
    fechaSaldosMovsContainer.style.display = 'none';
}

function showFechaMovsContainer() {
    fechaMovsContainer.style.display = '';
    fechaSaldosContainer.style.display = 'none';
    fechaSaldosMovsContainer.style.display = 'none';
}

function showFechaSaldosMovsContainer() {
    fechaSaldosMovsContainer.style.display = '';
    fechaSaldosContainer.style.display = 'none';
    fechaMovsContainer.style.display = 'none';
}
////////////////////////////////////////////////////////

////////////////////////////////////////////////////////
//Contenedores por tipo de mes
function showMesSaldosContainer() {
    mesSaldosContainer.style.display = '';
    mesMovsContainer.style.display = 'none';
    mesSaldosMovsContainer.style.display = 'none';
}

function showMesMovsContainer() {
    mesMovsContainer.style.display = '';
    mesSaldosContainer.style.display = 'none';
    mesSaldosMovsContainer.style.display = 'none';
}

function showMesSaldosMovsContainer() {
    mesSaldosMovsContainer.style.display = '';
    mesSaldosContainer.style.display = 'none';
    mesMovsContainer.style.display = 'none';
}
////////////////////////////////////////////////////////

////////////////////////////////////////////////////////
//Contenedores por tipo de año
function showAnSaldosContainer() {
    anSaldosContainer.style.display = '';
    anMovsContainer.style.display = 'none';
    anSaldosMovsContainer.style.display = 'none';
}

function showAnMovsContainer() {
    anMovsContainer.style.display = '';
    anSaldosContainer.style.display = 'none';
    anSaldosMovsContainer.style.display = 'none';
}

function showAnSaldosMovsContainer() {
    anSaldosMovsContainer.style.display = '';
    anSaldosContainer.style.display = 'none';
    anMovsContainer.style.display = 'none';
}
////////////////////////////////////////////////////////

//Función para cerrar contenedores del perfil Comercial
//function closeContainersComercial() {
    //fechaSaldosContainer.style.display = 'none';
    //fechaMovsContainer.style.display = 'none';
    //fechaSaldosMovsContainer.style.display = 'none';
    //mesSaldosContainer.style.display = 'none';
    //mesMovsContainer.style.display = 'none';
    //mesSaldosMovsContainer.style.display = 'none';
    //anSaldosContainer.style.display = 'none';
    //anMovsContainer.style.display = 'none';
    //anSaldosMovsContainer.style.display = 'none';
//}
////////////////////////////////////////////////////////
////////////////////////////////////////////////////////


////////////////////////////////////////////////////////
//LÓGICA PARA CONTENEDORES DE PERFIL ADMINISTRATIVO
//Contenedor principal de gestión de usuarios
function showAdminContainer() {
    adminContainer.style.display = '';
    menuContainer.style.display = 'none';
}
//Contenedor principal de generación de certificados
function showComContainer() {
    comercialContainer.style.display = '';
    menuContainer.style.display = 'none';
}
////////////////////////////////////////////////////////

//Contenedor principal para inscripción de auxiliar administrativo
function showAuxContainer() {
    auxContainer.style.display = '';
    asesorContainer.style.display = 'none';
    updateContainer.style.display = 'none';
    deleteContainer.style.display = 'none';
    logContainer.style.display = 'none';
}

//Contenedor principal para inscripción de asesor comercial
function showComercialContainer() {
    asesorContainer.style.display = '';
    auxContainer.style.display = 'none';
    updateContainer.style.display = 'none';
    deleteContainer.style.display = 'none';
    logContainer.style.display = 'none';
}

////////////////////////////////////////////////////////
//Contenedor principal para actualización de usuarios
function showUpdateContainer() {
    updateContainer.style.display = '';
    auxContainer.style.display = 'none';
    asesorContainer.style.display = 'none';
    deleteContainer.style.display = 'none';
    logContainer.style.display = 'none';
}
//Subcontenedores de actualización de usuarios

//Subcontenedor de actualización de auxiliar administrativo
function showUpdateAdminContainer() {
    updateAdminContainer.style.display = '';
    updateComercialContainer.style.display = 'none';
}
//Subcontenedor de actualización de asesor comercial
function showUpdateComercialContainer() {
    updateComercialContainer.style.display = '';
    updateAdminContainer.style.display = 'none';
}
////////////////////////////////////////////////////////

//Contenedor principal para eliminar usuarios
function showDeleteContainer() {
    deleteContainer.style.display = '';
    auxContainer.style.display = 'none';
    asesorContainer.style.display = 'none';
    updateContainer.style.display = 'none';
    logContainer.style.display = 'none';
}

////////////////////////////////////////////////////////

//Contenedor principal para ver el histórico
function showLogContainer() {
    logContainer.style.display = '';
    auxContainer.style.display = 'none';
    asesorContainer.style.display = 'none';
    updateContainer.style.display = 'none';
    deleteContainer.style.display = 'none';
}

//Función para cerrar contenedores del perfil Administrativo
//function closeContainersAdmin(){
    //auxContainer.style.display = 'none';
    //asesorContainer.style.display = 'none';
    //updateContainer.style.display = 'none';
    //deleteContainer.style.display = 'none';
    //updateAdminContainer.style.display = 'none';
    //updateComercialContainer.style.display = 'none';
    //logContainer.style.display = 'none';
//}
