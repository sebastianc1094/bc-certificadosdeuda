//Función para bloquear números o letras dependiendo del campo de texto
var numbers = /[1234567890]/g;
var keys = /[A-Za-z]/g;

function blockKeyboard(myfield, e, restrictionType) {
    if (!e) var e = window.event;
    if (e.keyCode) code = e.keyCode;
    else if (e.which) code = e.which;

    var character = String.fromCharCode(code);
    
    if (code == 27) { 
        this.blur(); 
        return false; 
    }

    if (!e.ctrlKey && code !=32) {
        if (character.match(restrictionType)) {
            return true;
        } else {
            return false;
        }
    }
}