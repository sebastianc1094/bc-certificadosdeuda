//Función para identificar si se está visualizando el sitio desde Internet Explorer
function getIEVersion() {
    var agent = window.navigator.userAgent;
    var index = agent.indexOf("MSIE");

    if (index > 0)
        return parseInt(agent.substring(index + 5, agent.indexOf(".", index)));

    else if (!!navigator.userAgent.match(/Trident\/7\./))
        return 11;
}

if (getIEVersion() > 0)
    alert("Este sitio no es compatible con Internet Explorer, por favor utilice Google Chrome o Microsoft Edge");
