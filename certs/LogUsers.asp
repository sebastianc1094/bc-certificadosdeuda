<html>
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="../img/icon.ico">
    <link rel="stylesheet" type="text/css" href="../css/animate.css">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <script type="text/javascript" src="../js/jquery-2.1.3.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/jspdf.debug.js"></script>
    <script type="text/javascript" src="../js/jspdf-autotable-2.3.2.js"></script>}
    <script type="text/javascript" src="../js/LogSearch.js"></script>
    <title>Histórico de usuarios</title>
</head>
<body>
    <%
        perfil = "0"
        user = lcase(request.serverVariables("AUTH_USER"))
		Set cnx = Server.CreateObject("ADODB.Connection")
		cnx.Open "Provider=Microsoft.Jet.OLEDB.4.0; DATA SOURCE=" & server.mappath("DB.mdb")

		Set rs = Server.CreateObject("ADODB.RecordSet")
        Set rsUser = server.createObject("ADODB.RecordSet")
		rs.Open "SELECT * FROM tbl_users", cnx
        rsUser.open "SELECT Nombre, Perfil FROM tbl_users WHERE UsuarioBC='"& user &"'", cnx

        if rsUser.EOF = false then
            perfil = rsUser.fields(1).value          
		end if
    %>
    <% if perfil = "Administrativo" then %>
        <nav class="navbar navbar-inverse navbar-fixed-top" style="background-color:#073D86">
            <div class="container-fluid">
                <div class="navbar-header">
                    <img src="../img/logo-banner.png" style="width:200px; height:50px; margin-top:20px; margin-bottom:10px; margin-left:15px;">
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <div class="nav navbar-nav navbar-right">              
                        <h4 class="label" style="color:#073D86; font-size:18px; background-color:#FFD302; margin:15px; padding-top:10px"><span class="glyphicon glyphicon-user" style="font-size:25px; margin-right:10px; margin-top:20px" aria-hidden="true"></span>Usuario: <%=Response.write(rs.fields(0).value)%> Rol: <%=perfil%></h4>
                    </div>
                </div>
            </div>
        </nav>     
        <div class="animated fadeIn" style="margin:55px; margin-top:110px">      
            <div style="float:right">
                <a href="../Index.asp" style="margin-left:180px; text-decoration:none; font-size:25px; color:#073D86">Volver a inicio<span class="glyphicon glyphicon-home" style="margin-left:10px"></span></a>
            </div>          
            <h1>Histórico de usuarios registrados</h1>
            <h4 style="margin-bottom:25px">A continuación encontrará el histórico del registro de usuarios</h4>
            <form>
                <label for="searchTerm" style="font-size:16px; margin-right:10px">Buscar registro</label><input id="searchTerm" type="text" onkeyup="doSearch()"/>                
            </form>
            <table id="data" class="table table-bordered table-striped">
                    <thead class="thead-dark">
                        <tr>
                            <th style="text-align:center; vertical-align:top; background-color:#FFD302; color:#073D86; font-size:18px">Nombre del<br>usuario</th>
                            <th style="text-align:center; vertical-align:top; background-color:#FFD302; color:#073D86; font-size:18px">Documento del<br>usuario</th>
                            <th style="text-align:center; vertical-align:top; background-color:#FFD302; color:#073D86; font-size:18px">Área</th>
                            <th style="text-align:center; vertical-align:top; background-color:#FFD302; color:#073D86; font-size:18px">Perfil</th>
                            <th style="text-align:center; vertical-align:top; background-color:#FFD302; color:#073D86; font-size:18px">Usuario de <br>red Bancolombia</th>         
                            <th style="text-align:center; vertical-align:top; background-color:#FFD302; color:#073D86; font-size:18px">Fecha y hora<br>de registro</th>                      
                        </tr>
                    </thead>
                    <tbody>
                        <% Do Until rs.EOF
                            Response.Write "<tr>" & vbNewLine
                        For fnum = 0 To rs.Fields.Count-1
                            Response.Write "<td>" & rs.Fields(fnum).Value & "</td>" & vbNewLine
                        Next
                        Response.Write "</tr>" & vbNewLine
                        rs.MoveNext
                        Loop
                        %>
                    </tbody>
            </table>
        </div>      
    <% end if %>      
    <% if perfil = "0" then %>
        <script language="Javascript">
            $(document).ready(function(){
                $("#showMod").modal("show");
            });
        </script>
        <div class="modal fade" id="showMod" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <img src="../img/logo.png" style="float:left; width:140px; height:35px">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h3 class="modal-title">Revise sus credenciales de acceso<span class="glyphicon glyphicon-remove" style="margin-left:10px"></span></h3>
                        <h4>No dispone de los permisos administrativos para ingresar al portal.</h4>
                        <h4>Por favor contacte a su auxiliar administrativo para solicitar el acceso.</h4>
                    </div>
                    <div class="modal-footer">
                        <a style="text-decoration:none; font-size:20px; color:black" href="mailto:sebastian.castanomo@upb.edu.co?subject=Solicitud de acceso a plataforma de generación de certificados de deuda&body=DATOS DE INSCRIPCIÓN A PORTAL DE GENERACIÓN DE CERTIFICADOS DE DEUDA %0D%0A %0D%0ANOMBRES Y APELLIDOS COMPLETOS: %0D%0ADOCUMENTO DE IDENTIDAD: %0D%0AAREA: %0D%0AUSUARIO DE RED BANCOLOMBIA: ">Solicitar acceso<span class="glyphicon glyphicon-envelope" style="margin-left:10px"></span></a>
                    </div>
                </div>
            </div>
        </div>
    <% end if %>
    <%
        rs.Close
        rsUser.Close
		cnx.Close
    %>       
</body>
<html>