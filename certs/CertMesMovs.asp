<html>
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="../img/icon.ico">
    <link rel="stylesheet" type="text/css" href="../css/animate.css">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <script type="text/javascript" src="../js/jquery-2.1.3.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script> 
    <script type="text/javascript" src="../js/jspdf.debug.js"></script>
    <script type="text/javascript" src="../js/jspdf-autotable-2.3.2.js"></script>
    <script src="../js/generatePDF.js"></script>
    <title>Movimientos a la fecha</title>
</head>
<body style="background:lightgray">
    <%
        identificacion = Request.form("identificacion")
        mes = Request.form("mes")
        user = lcase(request.serverVariables("AUTH_USER"))

        Set cnx = Server.CreateObject("ADODB.Connection")
        cnx.Open = "Provider=Microsoft.Jet.OLEDB.4.0; DATA SOURCE=" & Server.mappath("DB.mdb")

        Set movimientos = Server.CreateObject("ADODB.RecordSet")
        Set cliente = Server.CreateObject("ADODB.RecordSet")
        Set datos = Server.CreateObject("ADODB.RecordSet")
        Set fechaCorte = Server.CreateObject("ADODB.RecordSet")

        movimientos.Open "SELECT NumPrestamo, DescripcionTransac, cdate(mid(FechaEfectiva,5,2)+'/'+right(FechaEfectiva,2)+'/'+left(FechaEfectiva,4)), format(ValTransac,'Currency') FROM tbl_movimientos_mes WHERE Identificacion= " & identificacion & " AND month(FechaCorteMes)="& mes &";", cnx
        cliente.Open "SELECT Nombre FROM tbl_saldos WHERE Identificacion= " & identificacion & ";", cnx
        datos.Open "SELECT Nombre, Perfil FROM tbl_users WHERE UsuarioBC='"& user &"';", cnx
        fechaCorte.Open "SELECT day(FechaCorteMes), month(FechaCorteMes), year(FechaCorteMes) FROM tbl_movimientos_mes WHERE Identificacion = " & identificacion & "AND month(FechaCorteMes)="& mes &";", cnx
    %>   
    <% if movimientos.EOF = true then %>
        <script language="Javascript">
            $(document).ready(function(){
                $("#showMod").modal("show");
            });
        </script>
        <div class="modal fade" id="showMod" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <img src="../img/logo.png" style="float:left; width:140px; height:35px">
                    </div>
                    <div class="modal-body">
                        <h3 class="modal-title">Registro no encontrado<span class="glyphicon glyphicon-remove" style="margin-left:10px"></span></h3>
                        El número de identificación que ha ingresado no se encuentra, por favor verifique la información ingresada. 
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn" onclick="window.location.href='../Index.asp'" style="background-color:#073D86; color:white" data-dismiss="modal">Ir a inicio</button>
                        <h4 style="display:none"><%=movimientos%></h4>
                    </div>
                </div>
            </div>
        </div>
    <% end if %>
    <%   
        nomCliente = cliente.fields(0).value
        nombreAsesor = datos.fields(0).value
        perfilAsesor = datos.fields(1).value
        fechaDia = fechaCorte.fields(0).value
        fechaMes = fechaCorte.fields(1).value
        fechaAn = fechaCorte.fields(2).value
    %>
    <div>
        <h2 align="center"><span class="glyphicon glyphicon-list-alt" style="margin-right:10px"></span>Vista previa de certificado movimientos a la fecha<a href="../Index.asp" style="margin-left:180px; text-decoration:none;">Inicio<span class="glyphicon glyphicon-home" style="margin-left:10px"></span></a></h2>
    </div>   
    <div class="animated fadeIn" style="margin-left:80px; margin-right:80px; margin-top:30px; background:white; box-shadow: 5px 5px 10px #999">
        <div style="padding:50px">
            <img src="../img/logo.png" style="width:315px; height:80px; margin-bottom:50px">
            <br>
            <h4 id="date">Medellín, <script type="text/javascript" src="../js/Date.js"></script></h4>
            <br>
            <h4 id="title1">Señores</h4>
            <h4 id="title2"><%=nomCliente%></h4>
            <h4 id="title3">Identificación: <%=identificacion%></h4>
            <h4 id="title4">Colombia</h4>
            <br>
            <h4 id="title5">Referencia: Certificado de deuda moneda extranjera</h4>
            <br>
            <h4 id="title6">Reciba un cordial saludo.</h4>
            <br>
            <h4>Para Bancolombia es muy importante entregarle información clara, confiable y oportuna. Por eso le informamos que los movimientos en sus obligaciones por concepto de préstamos de moneda extranjera al cierre del <%=fechaDia%> de <%Response.write(MonthName(fechaMes))%> del <%=fechaAn%> son los siguientes:</h4>
            <h4 id="text1" style="display:none">Para Bancolombia es muy importante entregarle información clara, confiable y oportuna. Por eso</h4>
            <h4 id="text2" style="display:none">le informamos que los movimientos en sus obligaciones por concepto de préstamos de moneda</h4>
            <h4 id="text3" style="display:none">extranjera al cierre del <%=fechaDia%> de <%Response.write(MonthName(fechaMes))%> del <%=fechaAn%> son los siguientes:</h4>
            <br>
            <table id="result" class="table table-bordered table-striped">
                <thead class="thead-dark">
                    <th>Número de préstamo</th>
                    <th>Descripción de transacción</th>
                    <th>Fecha de abono</th> 
                    <th>Monto transacción (USD)</th>  
                </thead>
                <tbody>
                    <%						
                    do until movimientos.EOF
                        Response.Write "<tr>" & vbNewLine
                    for fields = 0 to movimientos.Fields.Count-1
                        Response.Write "<td>" & movimientos.Fields(fields).Value & "</td>" & vbNewLine
                    next
                        Response.Write "</tr>" & vbNewLine
                        movimientos.MoveNext
                    loop                   
                    %>				
                </tbody>
            </table>
            <table id="sign1">
                <thead>
                    <tr>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="height:50px">*Montos en dólares americanos.</td>
                    </tr>
                    <tr>
                        <td>Cualquier inquietud será atendida por su gerente de Moneda Extranjera.</td>
                    </tr>
                <tbody>
            </table>
            <table id="sign2">
                <thead>
                    <tr>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="height:100px">Atentamente,</td>
                    </tr>
                <tbody>
            </table>
            <table id="sign4">
                <thead>
                    <tr>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td align="right"></td>
                    <td>
                        <img style="width:135px; height:40px;" src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDABALDA4MChAODQ4SERATGCgaGBYWGDEjJR0oOjM9PDkzODdASFxOQERXRTc4UG1RV19iZ2hnPk1xeXBkeFxlZ2P/2wBDARESEhgVGC8aGi9jQjhCY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2P/wAARCABZAW4DASIAAhEBAxEB/8QAGwABAAMBAQEBAAAAAAAAAAAAAAEEBQMGAgf/xAA0EAABBAIABAQFBAEDBQAAAAABAAIDBAURBhIhMRNBUWEUIjJxgSMzQlIVYpGhJENTcsH/xAAVAQEBAAAAAAAAAAAAAAAAAAAAAf/EABYRAQEBAAAAAAAAAAAAAAAAAAABEf/aAAwDAQACEQMRAD8A/QFCKCQO6CVwjtMllkYzr4Z04+hXdYXDTpHm/wCKST451tBuqVClARQiCUUIglFClBCIiAiIgKVClAUIiDJ4ha11eu1x0DM3e1oV2GNnJ5Dt9lncTM3iy8d43td/ytCB3OyKUHo5oVHdERQEREBQ7fKdd1KIKVO6ZbU9aTQlhI7eY9VdWPaArcQ15R08dhY73K2EBERAREQSiIgIiICIiCEUOcGNLnHQA2SsyhYORsPsNcRAw8rAP5e6DUREQEREHKzYZWgdLIeg/wCVzqMe8ePLsOeOjf6hVLX/AFeWirHqyJviOHqfJanYICxsE0R2shHvr4u9LZWJWd4HEs8flMzmCo20RFAREQEREBERAREQEREBERB8Syxwt5pXtY31J0vppDhsHYPYrIzlcXLNOs8/pueS5vrpdsY19SSSnI4ua35oyf6+io75WHx8bPHre2FcsTL4mIgf3LW6/wBlfcNtI9QsfAycvxVM94ZDr7FBsjqEXOB3NH1OyDorooCIiAiIgxOJwWVq9gHXgzAk+y2Y3B7GuHYjaoZ+Hx8NZYO/LsfhTgZ/iMPWfvZ5NH7oNBERAREQEXOaxDANyyNYPcr6Y9r2hzHBwPYhB9IiIClQiDH4msuhx3gxnUs7gxqvYyo2jQigb/FvX7+aybQ+O4phhPVlZvOfuvQKqIiKIIiIMirscQ2w4HZjbr7LXWPcBrZ6vPvTJW+GfuthAWFmYxSuVb7B2fyPPsVuqhm4PiMXO3zA5h9wkF4EEAjzUqlh7HxWMgk8+XRV1ARFznsRV4+eZwa3etlB0RQCCNjsVKAilQgIiICIiAiIgzr/AMuRpO/1EL7yA8OSCx/R2j9iueaj/RimHeKQO/C73m+Nj5NebdhUWx1G1gWHDH8TRyHpHabyk+62acni1In+rQsrimuX49thn113B/4RWnCdWJme4IXckNBJ6ALOo2mWJIZmn96JdM1I6LE2HsOiGIipLxDDHKdQSvhadGVo6LVrzx2YWywuDmOGwQqmKhj/AMTAzQc1zOvTuq2CDYJrlVp+WN+wPQFBsIqWTylfGRtdOSS86a1o2SVmnJZm11qUGxsPZ0pUMbkzBJC9h7OBC8/wjNystUnHrBIdD2XVlbiCbrNbgh9mN2sqlTtVOKJoH2i10zeYvaPqVV7NFw61oS58heR3Ll3B2NqIeelxtWGVa75pCA1g31WTat+FxCSS4shgJcB5kqrmrxv46o0MdGJ5gC0+YBVxcXcfRF2I2r48V0p21p7NHkpx4NLLS0Wn9FzfEYD5ey1mNDGNaOwGljRP+I4qkLOrYIeUny2UG2pUKVEFClfL/oP2QYXD7fGyORtHuZOQfYLeWHwto1rLh3MztrdQQiKUEIiIM/NV3T0SYx+pGedv4VihYFqpHKD1I6/dWCNjRWNC92NyhgeNVpztjvQ+iDZUPaHNLT2I0pRBg8OPMNi7Rd/2pNt+xW3L+0//ANSsKY/A8Vxv6BlpnKfuvQdwqPM8N3pvjpas8hc1w52c3fv1WrnoXTYyQM+pnzD8LzFV8lbi9sUmwA8gfYr2k8fiwSRn+TSEqqmEs/FYuGQkE60dK89wYwucdADZK87wjL4bLVF5+eGQ9D6L0T2h7C09iNIj4rzx2YhLC4OYexCq5e5LRpGeJnOWkbHsquCHw77FM/wfzNHsVp2YhNXfG4bDh2QKs7bNaOZvZ7dro5wY0ud0AGysvh+dstR8Q6GJ5br0Wo5oe0tPYjRUHxBPHYibLE7mY7sV0WXhmfDPsU//ABv5m/YrUQEREHK3EJ60kZ/k0rlScJaTQep1ylWlRhPw118JGmSfMw+/mg+cK/mquZ5xvc1XJ4mzwPieNteCCqGMYYbt2M9i/mH5Wmg8jhppKuUbjpN7ieeX7FenvRCelNGf5MK85xK047K1MnH25tPC9LXmZZgZLGQ5jxsFWrVPASiTFRDzZth/Cq4Qc+TyU3kZA0Fcr85wLZ3jrFPssHo5XsBVdVxkfifuyfO8+5QXJasMszZZIw57PpJ8lWpTzT3bO/2WHlaumUtijQmnPdreg9SuOCiezGsfKSZJfndv3RGivM8VNNW7Rvs/g8NP2Xplk8TVTaw8oaNuZ84/CkItZH9TGTEddx8wX1jZvHx8Ev8AZgVfEWG5DDRk9yzkcPfsqWFtGpWt1JvqrFxaN92qqrtiltWctZhbzvaQ1g9dKvkstWmgpF36UsMo543DRHqtjhrb8aZy3Rmkc9WMniq2Qge18TPEI6P11BQU7ecEp+HxrTPO8dHAfK332reGx5oVSJHc80h5pHepWbhb8dN3+OusbBNH0a8jQeFr28lVqwGV8rTodADskoLaKpjZprFUTTs5HPOw30HkraiJUFSoQee4ePw2TyFNx68/O0ey9CsTMYuw+02/j3ctho0R/ZVjxFcg+Sxi5fEHfl7Kq9Im15r/ADuVs9KmLcP9T1P+Ozl8j4u42vGepZF3Qx6Fs0bnljXtLh3APUL7VPH42DHx6iBLz9T3HZKuKIKtfpsu1jG/oe7XDuCrShBnYy1Js1LPSaPz/sPVaKrWqon09p5JW/S4KK9lwd4VgBsg7Hycgy+LISKsNtn1wSA/hbVeQSwRyDs5oKr5aAWcXYiI3zMOlU4dsmTBROILnRtLSPMkKqx8/LHW4qpzAbLQDJ7L1rHtkYHtO2kbBCw8fi5LT7lrIM0+x8rWHuxvkuEVu3gHGvZiksVj+09g2R7FBXltR47jCR7TyxytDZd9gV6wEOAI6grzWPxjspFcs3YjGbJ+QHu0DsumPyMuKd8DlCQGnUU2uhHuhXSaQ1+K4W7AbNGfyt5eQyU0mTyrbdEF0dFu+YD6vsvT0rcdysyaJwII6j0PolGRC447iV8TukVsczfTYW+vNcUMlmtVm1etiIGQAegWxib7MhRZKD8+tPb5gpRxnf8AD5uB3TlnYWH7rTXn+JZHi3QbFsvDy7lC3IJRNC148wiOiIigKrfrGzBph5ZGnmYfQq0iDFxVrxsrPzEtk5AHMPkQtpZuQxLLUosQSGCy3tI3/wCqm5vEQ/SDq7h28Xz/ANlRGSgGWywpk7iijJf9z2VbA3Ti7D8Tedylp3E89iPRbOJx3wETjJIZZ5DzSSHzK+sjiqmSYG2Y9kdnNOiPyi6w2kcR5t7XsJpVemt9HOW1FSnrDkr2Nx+TZBvX5XWhQr4+DwazOVu9knuVaQeZz0c81unVmnBEsmy0DQDQvRV+XwW8v066LzedjtuzsHw8Tnc8fIHa6N33XpIIxFAyMfxACUdF8SsEsT2Hs4aX2iiMHhupaoS260zCIg/mY7yK+s1w+b8pnqzmCZw5XHycFuImrqvj6opUoq4O/DbrfqrCIiKd/F1MiwCzEHEdneYVWnw5j6kniNY6RwOxzu3payIHZERARSiCE0iIHZERAREQSoUoghV7lSO5CY3kj0cO4VlQg866TK4xjoZIjcrkENe36h91b4aqyVcZyytLXOeXaPcBa6BVRCAe4UooiNLnNBFYZyTRte30cF1RByhrxQR8kUbWN9AFmS4QssOno2X1nOOy0dWn8LYUIKFLGNrzPsSyOmsPGnPd6ewVO1ibFe2beKkbG937kTvpctxQgx6WNsyZAX8i9pka3lZGzs1aUMHgudyuPITvl9F2UoIRSiCEUoghFKIIRSiCEUogjSKUQQilEBQpRBCKUQEREBERAREQEREEIpRBCKUQf//Z"
                        />
                    </td>
                    </tr>
                </tbody>
            </table>               
            <table id="sign5">
                <thead>
                    <tr>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="height:30px">Jessica Mestre Loaiza</td>
                    </tr>
                    <tr>
                        <td style="height:30px">Jefe de sección</td>
                    </tr>
                    <tr>
                        <td style="height:30px">Sección financiación ML y ME</td>
                    </tr>
                    <tr>
                        <td style="height:30px">BANCOLOMBIA S.A.</td>
                    </tr>
                <tbody>
            </table> 

            <form id="log" style="display:none" action="Generate.asp" method="post">
                <input type="text" name="nombre" value="<%=nombreAsesor%>">
                <input type="text" name="user" value="<%=user%>">
                <input type="text" name="perfil" value="<%=perfilAsesor%>">
                <input type="text" name="tipo" value="Certificado a mes de movimientos"> 
                <input type="text" name="doc" value="<%=identificacion%>">       
            </form>

            <div style="margin:25px; margin-top:50px">
                <button type="submit" form="log" class="btn" style="color:white; background-color: #073D86; font-size:18px" onclick="generateMovs()"><span class="glyphicon glyphicon-save-file"></span>Descargar certificado</button>
            </div>
            <%               
                movimientos.Close 
                cliente.Close
                datos.Close
                fechaCorte.Close
                cnx.Close
            %>
        </div>
    </div>
</body>
<html>