<html>
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="img/icon.ico">
    
    <link rel="stylesheet" type="text/css" href="css/animate.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/steps.css" >

    <script type="text/javascript" src="js/jquery-2.1.3.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>  
    <script type="text/javascript" src="js/tab.js"></script>
    <script type="text/javascript" src="js/Tabs.js"></script>       
    <script type="text/javascript" src="js/Browser.js"></script>
    <script type="text/javascript" src="js/KeyboardValidation.js"></script>
    <script type="text/javascript" src="js/Steps.js"></script>
    <script type="text/javascript" src="js/Containers.js"></script>
    <title>Certificados de deuda ME</title>
</head>
<body>
    <%
        perfil = "0"
        'user = lcase(request.serverVariables("AUTH_USER"))
        user = "bancolombia\secastan"
		Set cnx = server.createObject("ADODB.Connection")
		cnx.open = "Provider=Microsoft.Jet.OLEDB.4.0; DATA SOURCE=" & server.mappath("certs/DB.mdb")

		Set rs = server.createObject("ADODB.RecordSet")
		rs.open "SELECT Nombre, Perfil FROM tbl_users WHERE UsuarioBC='"& user &"'", cnx	
               
		if rs.EOF = false then
            perfil = rs.fields(1).value          
		end if       
    %>

    <% if perfil = "Administrativo" then %>
        <nav class="navbar navbar-inverse navbar-fixed-top" style="background-color:#073D86">
            <div class="container-fluid">
                <div class="navbar-header">
                    <img src="img/logo-banner.png" style="width:200px; height:50px; margin-top:15px; margin-bottom:10px; margin-left:15px">
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <div class="nav navbar-nav navbar-right">              
                        <h4 class="label" style="color:#073D86; font-size:18px; background-color:#FFD302; margin:15px; padding-top:10px"><span class="glyphicon glyphicon-user" style="font-size:25px; margin-right:10px; margin-top:20px" aria-hidden="true"></span>Usuario: <%=Response.write(rs.fields(0).value)%> Rol: <%=perfil%></h4>
                    </div>
                </div>
            </div>
        </nav>
        <div id="menuContainer" class="animated fadeIn" style="margin:55px; margin-top:150px">
            <h1>Portal administrativo</h1>
            <h3>Seleccione una opción</h3>
                       
            <div class="wizard">
                <div class="wizard-inner">                   
                    <ul class="nav nav-tabs">  
                        <li>
                            <a onclick="showAdminContainer()" title="Gestionar usuarios y ver histórico de registros">
                                <span class="round-tab">
                                    <i class="glyphicon glyphicon-user" style="margin:30%"></i>
                                </span>
                            </a>
                        </li>
            
                        <li>
                            <a onclick="showComContainer()" title="Generar certificados">
                                <span class="round-tab">
                                    <i class="glyphicon glyphicon-save-file" style="margin:30%"></i>
                                </span>
                            </a>
                        </li>                                   
                    </ul>
                </div>
            </div>         
        </div>

        <div id="adminContainer" class="animated fadeIn" style="margin:85px; display:none; margin-top:110px">
            <div style="float:right">
                <a href="./Index.asp" style="margin-left:180px; text-decoration:none; font-size:25px; color:#073D86">Volver a inicio<span class="glyphicon glyphicon-home" style="margin-left:10px"></span></a>
            </div>         
            <h2>Gestión de usuarios e histórico</h2>
            <h4>Seleccione una opción</h4>

            <div style="padding:15px">  
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#aux" aria-controls="aux" data-toggle="tab" style="font-size:15px; color:#073D86"><span class="glyphicon glyphicon-user" style="margin-right:10px"></span>Inscribir auxiliar administrativo</a>
                    </li>
                    <li>
                        <a href="#com" aria-controls="com" data-toggle="tab" style="font-size:15px; color:#073D86"><span class="glyphicon glyphicon-user" style="margin-right:10px"></span>Inscribir asesor comercial</a>
                    </li>
                    <li>
                        <a href="#update" aria-controls="update" data-toggle="tab" style="font-size:15px; color:#073D86"><span class="glyphicon glyphicon-pencil" style="margin-right:10px"></span>Actualizar datos de usuario</a>
                    </li>
                    <li>
                        <a href="#delete" aria-controls="delete" data-toggle="tab" style="font-size:15px; color:#073D86"><span class="glyphicon glyphicon-remove" style="margin-right:10px"></span>Eliminar usuario</a>
                    </li>
                    <li>
                        <a href="#log" aria-controls="log" data-toggle="tab" style="font-size:15px; color:#073D86"><span class="glyphicon glyphicon-list-alt" style="margin-right:10px"></span>Histórico</a>
                    </li>
                </ul>
            
                <div class="tab-content">
                    <div id="aux" class="tab-pane active">
                        <div class="animated fadeIn" style="margin:30px">
                            <h4 class="label label-info" style="font-size:20px; color:#073D86; background-color:#FFD302"><span class="glyphicon glyphicon-user" style="margin-right:10px; color:#073D86"></span>Inscripción de auxiliar administrativo</h4>
                            <div style="margin:40px">
                                <form action="InsertAdmin.asp" method="post">
                                    <table>
                                        <tr>
                                            <h4>Nombres y apellidos<span class="glyphicon glyphicon-user" style="margin-left:10px"></span></h4>
                                            <input type="text" required name="nombre" class="form-control" onkeypress="return blockKeyboard(this, event, keys);" placeholder="Nombres y apellidos completos del auxiliar administrativo">
                                        </tr>
                                        <tr>
                                            <h4>Documento de identidad<span class="glyphicon glyphicon-credit-card" style="margin-left:10px"></span></h4>
                                            <input type="text" required name="identificacion" class="form-control" onkeypress="return blockKeyboard(this, event, numbers);" placeholder="Documento de identidad del auxiliar administrativo">
                                        </tr>
                                        <tr>
                                            <h4>Nombre de usuario Bancolombia<span class="glyphicon glyphicon-globe" style="margin-left:10px"></span></h4>
                                            <input type="text" required name="user" class="form-control" onkeypress="return blockKeyboard(this, event, keys);" placeholder="Usuario de red del auxiliar administrativo">
                                        </tr>
                                        <tr>
                                            <button type="submit" class="btn" style="margin-top:15px; float:right; background-color:#073D86; color:white"><span class="glyphicon glyphicon-floppy-disk" style="margin-right:10px"></span>Inscribir auxiliar administrativo</button>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div id="com" class="tab-pane">
                        <div class="animated fadeIn" style="margin:30px">
                            <h4 class="label label-info" style="font-size:20px; color:#073D86; background-color:#FFD302"><span class="glyphicon glyphicon-user" style="margin-right:10px; color:#073D86"></span>Inscripción de auxiliar administrativo</h4>
                            <div style="margin:40px">
                                <form action="InsertCom.asp" method="post">
                                    <table>
                                        <tr>
                                            <h4>Nombre y apellidos<span class="glyphicon glyphicon-user" style="margin-left:10px"></span></h4>
                                            <input type="text" required name="nombre" class="form-control" onkeypress="return blockKeyboard(this, event, keys);" placeholder="Nombres y apellidos completos del asesor comercial">
                                        </tr>
                                        <tr>
                                            <h4>Documento de identidad<span class="glyphicon glyphicon-credit-card" style="margin-left:10px"></span></h4>
                                            <input type="text" required name="identificacion" class="form-control" onkeypress="return blockKeyboard(this, event, numbers);" placeholder="Documento de identidad del asesor comercial">
                                        </tr>
                                        <tr>
                                            <h4>Área<span class="glyphicon glyphicon-pencil" style="margin-left:10px"></span></h4>
                                            <input type="text" required name="area" class="form-control" onkeypress="return blockKeyboard(this, event, keys);" placeholder="Área del asesor comercial">
                                        </tr>
                                        <tr>
                                            <h4>Nombre de usuario Bancolombia<span class="glyphicon glyphicon-globe" style="margin-left:10px"></span></h4>
                                            <input type="text" required name="user" class="form-control" onkeypress="return blockKeyboard(this, event, keys);" placeholder="Usuario de red del asesor comercial">
                                        </tr>
                                        <tr>
                                            <button type="submit" class="btn" style="margin-top:15px; float:right; background-color:#073D86; color:white"><span class="glyphicon glyphicon-floppy-disk" style="margin-right:10px"></span>Inscribir asesor comercial</button>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div id="update" class="tab-pane">
                        <div class="animated fadeIn" style="margin:30px">
                            <h4 class="label label-info" style="font-size:20px; color:#073D86; background-color:#FFD302"><span class="glyphicon glyphicon-pencil" style="margin-right:10px; color:#073D86"></span>Actualizar datos de usuario</h4>
                            <div style="margin:40px">

                                <div class="btn-group btn-group-justified animated fadeIn" style="margin-top:20px">
                                    <a onclick="showUpdateAdminContainer()" class="btn btn-default" style="font-size:18px"><span class="glyphicon glyphicon-user" style="margin-right:10px"></span>Auxiliar administrativo</a>
                                    <a onclick="showUpdateComercialContainer()" class="btn btn-default" style="font-size:18px"><span class="glyphicon glyphicon-user" style="margin-right:10px"></span>Asesor comercial</a>
                                </div>

                                <div id="updateAdminContainer" class="animated fadeIn" style="display:none; margin-top:25px">                       
                                    <h4 class="label" style="font-size:20px; color:#073D86"><span class="glyphicon glyphicon-pencil" style="margin-right:10px; color:#073D86"></span>Actualizar auxiliar administrativo</h4>
                                    <form action="FormAdm.asp" method="post">
                                        <table>
                                            <tr>
                                                <h4 style="margin-top:25px">Documento de identidad<span class="glyphicon glyphicon-credit-card" style="margin-left:10px"></span></h4>
                                                <input type="text" required name="identificacion" class="form-control" onkeypress="return blockKeyboard(this, event, numbers);" placeholder="Documento de identidad del auxiliar administrativo">
                                            </tr>
                                            <tr>
                                                <button type="submit" class="btn" style="margin-top:15px; float:right; background-color:#073D86; color:white"><span class="glyphicon glyphicon-search" style="margin-right:10px"></span>Buscar auxiliar administrativo</button>
                                            </tr>
                                        </table>
                                    </form>
                                </div>

                                <div id="updateComercialContainer" class="animated fadeIn" style="display:none; margin-top:25px">                       
                                    <h4 class="label" style="font-size:20px; color:#073D86"><span class="glyphicon glyphicon-pencil" style="margin-right:10px; color:#073D86"></span>Actualizar asesor comercial</h4>
                                    <form action="FormCom.asp" method="post">
                                        <table>
                                            <tr>
                                                <h4 style="margin-top:25px">Documento de identidad<span class="glyphicon glyphicon-credit-card" style="margin-left:10px"></span></h4>
                                                <input type="text" required name="identificacion" class="form-control" onkeypress="return blockKeyboard(this, event, numbers);" placeholder="Documento de identidad del asesor comercial">
                                            </tr>
                                            <tr>
                                                <button type="submit" class="btn" style="margin-top:15px; float:right; background-color:#073D86; color:white"><span class="glyphicon glyphicon-search" style="margin-right:10px"></span>Buscar asesor comercial</button>
                                            </tr>
                                        </table>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div id="delete" class="tab-pane">
                        <div class="animated fadeIn" style="margin:30px">
                            <h4 class="label label-info" style="font-size:20px; color:#073D86; background-color:#FFD302"><span class="glyphicon glyphicon-remove" style="margin-right:10px; color:#073D86"></span>Eliminar usuario</h4>
                            <div style="margin:40px">
                                <form action="DeleteUser.asp" method="post">
                                    <table>
                                        <tr>
                                            <h4>Documento de identidad<span class="glyphicon glyphicon-credit-card" style="margin-left:10px"></span></h4>
                                            <input type="text" required name="identificacion" class="form-control" onkeypress="return blockKeyboard(this, event, numbers);" placeholder="Documento de identidad del usuario que desea eliminar">
                                        </tr>
                                        <tr>
                                            <button type="submit" class="btn" style="margin-top:15px; float:right; background-color:#073D86; color:white"><span class="glyphicon glyphicon-remove" style="margin-right:10px"></span>Eliminar usuario</button>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div id="log" class="tab-pane">
                        <div class="animated fadeIn" style="margin:30px">
                            <h4 class="label label-info" style="font-size:20px; color:#073D86; background-color:#FFD302"><span class="glyphicon glyphicon-list-alt" style="margin-right:10px; color:#073D86"></span>Histórico de registros</h4>
                            <div style="margin:40px">
                                <div class="btn-group btn-group-justified animated fadeIn" style="margin-top:20px">
                                    <a href="certs/LogCerts.asp" class="btn btn-default" style="font-size:18px"><span class="glyphicon glyphicon-list-alt" style="margin-right:10px"></span>Histórico de certificados</a>
                                    <a href="certs/LogUsers.asp" class="btn btn-default" style="font-size:18px"><span class="glyphicon glyphicon-list-alt" style="margin-right:10px"></span>Histórico de usuarios registrados</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            
            </div>
        </div> 

        <div id="comercialContainer" class="animated fadeIn" style="margin:55px; display:none; margin-top:110px">            
            <div style="float:right">
                <a href="./Index.asp" style="margin-left:180px; text-decoration:none; font-size:25px; color:#073D86">Volver a inicio<span class="glyphicon glyphicon-home" style="margin-left:10px"></span></a>
            </div> 
            <h1>Certificados de deuda de préstamos en Moneda Extranjera</h1>
            <h4>Seleccione el periodo de corte para el certificado que desea generar:</h4>
            
            <div style="padding:15px">  
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#day" aria-controls="day" data-toggle="tab" style="font-size:15px; color:#073D86"><span class="glyphicon glyphicon-calendar" style="margin-right:10px"></span>Ayer</a>
                    </li>
                    <li>
                        <a href="#month" aria-controls="month" data-toggle="tab" style="font-size:15px; color:#073D86"><span class="glyphicon glyphicon-calendar" style="margin-right:10px"></span>Mes específico</a>
                    </li>
                    <li>
                        <a href="#year" aria-controls="year" data-toggle="tab" style="font-size:15px; color:#073D86"><span class="glyphicon glyphicon-calendar" style="margin-right:10px"></span>Anual</a>
                    </li>
                </ul>
            
                <div class="tab-content">
                    <div id="day" class="tab-pane active">
                        <div class="animated fadeIn" style="margin:30px">
                            <h4 class="label label-info" style="font-size:20px; color:#FFD200; background-color:#5B5B5F"><span class="glyphicon glyphicon-calendar" style="margin-right:10px; color:#FFD200"></span>Certificados a ayer</h4>
                            <div style="margin:40px">
                                <h4 style="margin-top:20px">Seleccione el tipo de certificado: </h4>
                                
                                <div class="btn-group btn-group-justified" style="margin-top:15px">
                                    <a onclick="showFechaSaldosContainer()" class="btn btn-default" style="font-size:18px">Saldos a la fecha<span class="glyphicon glyphicon-menu-down" style="margin-left:10px"></span></a>
                                    <a onclick="showFechaMovsContainer()" class="btn btn-default" style="font-size:18px">Movimientos a la fecha<span class="glyphicon glyphicon-menu-down" style="margin-left:10px"></span></a>
                                    <a onclick="showFechaSaldosMovsContainer()" class="btn btn-default" style="font-size:18px">Saldos + Movimientos a la fecha<span class="glyphicon glyphicon-menu-down" style="margin-left:10px"></span></a>
                                </div>

                                <div id="fechaSaldosContainer" class="animated fadeIn" style="display:none">
                                    <form action="certs/CertFechaSaldos.asp" method="post">
                                        <table>
                                            <tr>
                                                <h4 style="margin-top:15px; color:#5B5B5F">Certificado de saldos a la fecha<span class="glyphicon glyphicon-list-alt" style="margin-left:10px"></span></h4>
                                                <h4 style="margin-top:20px; color:#5B5B5F">Identificación<span class="glyphicon glyphicon-user" style="margin-left:10px"></span></h4>
                                                <tr><input type="text" required name="identificacion" class="form-control" onkeypress="return blockKeyboard(this, event, numbers);" placeholder="Ingrese la cédula o NIT del cliente"></tr>
                                            </tr>
                                            <tr>
                                                <button type="submit" class="btn" style="margin-top:15px; float:right; font-size:16px; background-color:#073D86; color:white"><span class="glyphicon glyphicon-search" style="margin-right:10px"></span>Consultar</button>
                                            </tr>
                                        </table>
                                    </form>
                                </div>

                                <div id="fechaMovsContainer" class="animated fadeIn" style="display:none">
                                    <form action="certs/CertFechaMovs.asp" method="post">
                                        <table>
                                            <tr>
                                                <h4 style="margin-top:15px; color:#5B5B5F">Certificado de movimientos a la fecha<span class="glyphicon glyphicon-list-alt" style="margin-left:10px"></span></h4>
                                                <h4 style="margin-top:20px; color:#5B5B5F">Identificación<span class="glyphicon glyphicon-user" style="margin-left:10px"></span></h4>
                                                <tr><input type="text" required name="identificacion" class="form-control" onkeypress="return blockKeyboard(this, event, numbers);" placeholder="Ingrese la cédula o NIT del cliente"></tr>
                                            </tr>
                                            <tr>
                                                <button type="submit" class="btn" style="margin-top:15px; float:right; font-size:16px; background-color:#073D86; color:white"><span class="glyphicon glyphicon-search" style="margin-right:10px"></span>Consultar</button>
                                            </tr>
                                        </table>
                                    </form>
                                </div>

                                <div id="fechaSaldosMovsContainer" class="animated fadeIn" style="display:none">
                                    <form action="certs/CertFechaSaldosMovs.asp" method="post">
                                        <table>
                                            <tr>
                                                <h4 style="margin-top:15px; color:#5B5B5F">Certificado de saldos con movimientos a la fecha<span class="glyphicon glyphicon-list-alt" style="margin-left:10px"></span></h4>
                                                <h4 style="margin-top:20px; color:#5B5B5F">Identificación<span class="glyphicon glyphicon-user" style="margin-left:10px"></span></h4>
                                                <tr><input type="text" required name="identificacion" class="form-control" onkeypress="return blockKeyboard(this, event, numbers);" placeholder="Ingrese la cédula o NIT del cliente"></tr>
                                            </tr>
                                            <tr>
                                                <button type="submit" class="btn" style="margin-top:15px; float:right; font-size:16px; background-color:#073D86; color:white"><span class="glyphicon glyphicon-search" style="margin-right:10px"></span>Consultar</button>
                                            </tr>
                                        </table>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="month" class="tab-pane">
                        <div class="animated fadeIn" style="margin:30px">
                            <h4 class="label label-info" style="font-size:20px; color:#FFD200; background-color:#5B5B5F"><span class="glyphicon glyphicon-calendar" style="margin-right:10px; color:#FFD200"></span>Certificados a mes específico</h4>
                            <div style="margin:40px">
                                <h4 style="margin-top:20px">Seleccione el tipo de certificado: </h4>

                                <div class="btn-group btn-group-justified" style="margin-top:15px">
                                    <a onclick="showMesSaldosContainer()" class="btn btn-default" style="font-size:18px">Saldos a mes específico<span class="glyphicon glyphicon-menu-down" style="margin-left:10px"></span></a>
                                </div>

                                <div id="mesSaldosContainer" class="animated fadeIn" style="display:none">
                                    <form action="certs/CertMesSaldos.asp" method="post">
                                        <table>
                                            <tr>
                                                <h4 style="margin-top:15px">Certificado de saldos a mes específico (año vigente)<span class="glyphicon glyphicon-list-alt" style="margin-left:10px"></span></h4>
                                                <h4 style="margin-top:20px">Identificación<span class="glyphicon glyphicon-user" style="margin-left:10px"></span></h4>
                                                <input type="text" required name="identificacion" class="form-control" onkeypress="return blockKeyboard(this, event, numbers);" placeholder="Ingrese la cédula o NIT del cliente">
                                            </tr>
                                            <tr>
                                                <h4>Mes<span class="glyphicon glyphicon-calendar" style="margin-left:10px"></span></h4>
                                                <select name="mes" style="width:100%; height:35px; border-radius:4px">
                                                    <option value="01">Enero</option>
                                                    <option value="02">Febrero</option>
                                                    <option value="03">Marzo</option>
                                                    <option value="04">Abril</option>
                                                    <option value="05">Mayo</option>
                                                    <option value="06">Junio</option>
                                                    <option value="07">Julio</option>
                                                    <option value="08">Agosto</option>
                                                    <option value="09">Septiembre</option>
                                                    <option value="10">Octubre</option>
                                                    <option value="11">Noviembre</option>
                                                    <option value="12">Diciembre</option>
                                                </select>
                                            </tr>
                                            <tr>
                                                <button type="submit" class="btn" style="margin-top:15px; float:right; font-size:16px; background-color:#073D86; color:white"><span class="glyphicon glyphicon-search" style="margin-right:10px"></span>Consultar</button>
                                            </tr>
                                        </table>
                                    </form>
                                </div>

                                <div id="mesMovsContainer" class="animated fadeIn" style="display:none">
                                    <form action="certs/CertMesMovs.asp" method="post">
                                        <table>
                                            <tr>
                                                <h4 style="margin-top:15px">Certificado de movimientos a mes específico (año vigente)<span class="glyphicon glyphicon-list-alt" style="margin-left:10px"></span></h4>
                                                <h4 style="margin-top:20px">Identificación <span class="glyphicon glyphicon-user"></span></h4>
                                                <input type="text" required name="identificacion" class="form-control" onkeypress="return blockKeyboard(this, event, numbers);" placeholder="Ingrese la cédula o NIT del cliente">
                                            </tr>
                                            <tr>
                                                <h4>Mes<span class="glyphicon glyphicon-calendar" style="margin-left:10px"></span></h4>
                                                <select name="mes" style="width:100%; height:35px; border-radius:4px">
                                                    <option value="01">Enero</option>
                                                    <option value="02">Febrero</option>
                                                    <option value="03">Marzo</option>
                                                    <option value="04">Abril</option>
                                                    <option value="05">Mayo</option>
                                                    <option value="06">Junio</option>
                                                    <option value="07">Julio</option>
                                                    <option value="08">Agosto</option>
                                                    <option value="09">Septiembre</option>
                                                    <option value="10">Octubre</option>
                                                    <option value="11">Noviembre</option>
                                                    <option value="12">Diciembre</option>
                                                </select>
                                            </tr>
                                            <tr>
                                                <button type="submit" class="btn" style="margin-top:15px; float:right; font-size:16px; background-color:#073D86; color:white"><span class="glyphicon glyphicon-search" style="margin-right:10px"></span>Consultar</button>
                                            </tr>
                                        </table>
                                    </form>
                                </div>

                                <div id="mesSaldosMovsContainer" class="animated fadeIn" style="display:none">
                                    <form action="certs/CertMesSalMovs.asp" method="post">
                                        <table>
                                            <tr>
                                                <h4 style="margin-top:15px">Certificados de saldos + movimientos a mes específico (año vigente)<span class="glyphicon glyphicon-list-alt" style="margin-left:10px"></span></h4>
                                                <h4 style="margin-top:20px">Identificación <span class="glyphicon glyphicon-user"></span></h4>
                                                <tr><input type="text" required name="identificacion" class="form-control" onkeypress="return blockKeyboard(this, event, numbers);" placeholder="Ingrese la cédula o NIT del cliente"></tr>
                                            </tr>
                                            <tr>
                                                <h4>Mes<span class="glyphicon glyphicon-calendar" style="margin-left:10px"></span></h4>
                                                <select name="mes" style="width:100%; height:35px; border-radius:4px">
                                                    <option value="01">Enero</option>
                                                    <option value="02">Febrero</option>
                                                    <option value="03">Marzo</option>
                                                    <option value="04">Abril</option>
                                                    <option value="05">Mayo</option>
                                                    <option value="06">Junio</option>
                                                    <option value="07">Julio</option>
                                                    <option value="08">Agosto</option>
                                                    <option value="09">Septiembre</option>
                                                    <option value="10">Octubre</option>
                                                    <option value="11">Noviembre</option>
                                                    <option value="12">Diciembre</option>
                                                </select>
                                            </tr>
                                            <tr>
                                                <button type="submit" class="btn" style="margin-top:15px; float:right; font-size:16px; background-color:#073D86; color:white"><span class="glyphicon glyphicon-search" style="margin-right:10px"></span>Consultar</button>
                                            </tr>
                                        </table>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="year" class="tab-pane">
                        <div class="animated fadeIn" style="margin:30px">
                            <h4 class="label label-info" style="font-size:20px; color:#FFD200; background-color:#5B5B5F"><span class="glyphicon glyphicon-calendar"style="margin-right:10px; color:#FFD200"></span>Certificados a año específico</h4>
                                <div style="margin:40px">
                                    <h4 style="margin-top:20px">Seleccione el tipo de certificado: </h4>
                                    <div class="btn-group btn-group-justified" style="margin-top:15px">
                                        <a onclick="showAnSaldosContainer()" class="btn btn-default" style="font-size:18px">Saldos a año específico<span class="glyphicon glyphicon-menu-down" style="margin-left:10px"></a>
                                    </div>

                                    <div id="anSaldosContainer" class="animated fadeIn" style="display:none">
                                        <form action="certs/CertAnSaldos.asp" method="post">
                                            <table>
                                                <tr>
                                                    <h4 style="margin-top:15px">Certificado de saldos a año específico<span class="glyphicon glyphicon-list-alt" style="margin-left:10px"></span></h4>
                                                    <h4 style="margin-top:20px">Identificación<span class="glyphicon glyphicon-user" style="margin-left:10px"></span></h4>
                                                    <input type="text" required required name="identificacion" class="form-control" onkeypress="return blockKeyboard(this, event, numbers);" placeholder="Ingrese la cédula o NIT del cliente">
                                                </tr>
                                                <tr>
                                                    <h4>Año<span class="glyphicon glyphicon-calendar" style="margin-left:10px"></span></h4>
                                                    <select name="an" style="width:100%; height:35px; border-radius:4px">
                                                        <option value="2014">2014</option>
                                                        <option value="2015">2015</option>
                                                        <option value="2016">2016</option>
                                                        <option value="2017">2017</option>
                                                        <option value="2018">2018</option>
                                                    </select>
                                                </tr>
                                                <tr>
                                                    <button type="submit" class="btn" style="margin-top:15px; float:right; font-size:16px; background-color:#073D86; color:white"><span class="glyphicon glyphicon-search" style="margin-right:10px"></span>Consultar</button>
                                                </tr>
                                            </table>
                                        </form>
                                    </div>

                                    <div id="anMovsContainer" class="animated fadeIn" style="display:none">
                                        <form action="certs/CertAnMovs.asp" method="post">
                                            <table>
                                                <tr>
                                                    <h4 style="margin-top:15px">Certificado de movimientos a año específico<span class="glyphicon glyphicon-list-alt" style="margin-left:10px"></span></h4>
                                                    <h4 style="margin-top:20px">Identificación<span class="glyphicon glyphicon-user" style="margin-left:10px"></span></h4>
                                                    <input type="text" required name="identificacion" class="form-control" onkeypress="return blockKeyboard(this, event, numbers);" placeholder="Ingrese la cédula o NIT del cliente">
                                                </tr>
                                                <tr>
                                                    <h4>Año<span class="glyphicon glyphicon-calendar" style="margin-left:10px"></span></h4>
                                                    <select name="an" style="width:100%; height:35px; border-radius:4px">
                                                        <option value="2014">2014</option>
                                                        <option value="2015">2015</option>
                                                        <option value="2016">2016</option>
                                                        <option value="2017">2017</option>
                                                        <option value="2018">2018</option>
                                                    </select>
                                                </tr>
                                                <tr>
                                                    <button type="submit" class="btn" style="margin-top:15px; float:right; font-size:16px; background-color:#073D86; color:white"><span class="glyphicon glyphicon-search" style="margin-right:10px"></span>Consultar</button>
                                                </tr>
                                            </table>
                                        </form>
                                    </div>

                                    <div id="anSaldosMovsContainer" class="animated fadeIn" style="display:none">
                                        <form action="certs/CertAnSalMovs.asp" method="post">
                                            <table>
                                                <tr>
                                                    <h4 style="margin-top:15px">Certificado de saldos + movimientos a año específico<span class="glyphicon glyphicon-list-alt" style="margin-left:10px"></span></h4>
                                                    <h4 style="margin-top:20px">Identificación<span class="glyphicon glyphicon-user" style="margin-left:10px"></span></h4>
                                                    <input type="text" required name="identificacion" class="form-control" onkeypress="return blockKeyboard(this, event, numbers);" placeholder="Ingrese la cédula o NIT del cliente">
                                                </tr>
                                                <tr>
                                                    <h4>Año<span class="glyphicon glyphicon-calendar" style="margin-left:10px"></span></h4>
                                                    <select name="an" style="width:100%; height:35px; border-radius:4px">
                                                        <option value="2014">2014</option>
                                                        <option value="2015">2015</option>
                                                        <option value="2016">2016</option>
                                                        <option value="2017">2017</option>
                                                        <option value="2018">2018</option>
                                                    </select>
                                                </tr>
                                                <tr>
                                                    <button type="submit" class="btn" style="margin-top:15px; float:right; font-size:16px; background-color:#073D86; color:white"><span class="glyphicon glyphicon-search" style="margin-right:10px"></span>Consultar</button>
                                                </tr>
                                            </table>
                                        </form>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>    

            </div>
        </div> 
             
    <% end if %>

    <% if perfil = "Comercial" then %>
        <nav class="navbar navbar-inverse navbar-fixed-top" style="background-color:#00448C">
            <div class="container-fluid">
                <div class="navbar-header">
                    <img src="img/logo-banner.png" style="width:205px; height:50px; margin-top:15px; margin-bottom:10px; margin-left:15px">
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <div class="nav navbar-nav navbar-right">              
                        <h4 class="label" style="color:#073D86; font-size:18px; background-color:#FFD200; margin:15px; padding-top:10px"><span class="glyphicon glyphicon-user" style="font-size:25px; margin-right:10px; margin-top:20px"></span>Usuario: <%Response.Write rs.Fields(0).Value%> Rol: <%=perfil%></h4>
                    </div>
                </div>
            </div>
        </nav>
        <div id="comercialContainer" class="animated fadeIn" style="margin:55px; margin-top:110px">
            <div style="float:right; width: 450px; background-color:#FFD200; margin-bottom:10px; border-radius:10px"> 
                <h4 align="justify" style="margin:20px">NOTA: Recuerde que los certificados con corte de mes específico son meses del año vigente, y los certificados anuales son hasta los últimos cuatro años. Para generar certificados de meses específicos de años anteriores o de hace más de cuatro años, debe enviar la solicitud al correo de SECARCOM.</h4> 
            </div>              
            <h1>Certificados de deuda de préstamos en Moneda Extranjera</h1>
            <h4>Seleccione el periodo de corte para el certificado que desea generar:</h4>
            
            <div style="padding:15px">  
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#day" aria-controls="day" data-toggle="tab" style="font-size:15px; color:#073D86"><span class="glyphicon glyphicon-calendar" style="margin-right:10px"></span>Ayer</a>
                    </li>
                    <li>
                        <a href="#month" aria-controls="month" data-toggle="tab" style="font-size:15px; color:#073D86"><span class="glyphicon glyphicon-calendar" style="margin-right:10px"></span>Mes específico</a>
                    </li>
                    <li>
                        <a href="#year" aria-controls="year" data-toggle="tab" style="font-size:15px; color:#073D86"><span class="glyphicon glyphicon-calendar" style="margin-right:10px"></span>Anual</a>
                    </li>
                </ul>
            
                <div class="tab-content">
                    <div id="day" class="tab-pane active">
                        <div class="animated fadeIn" style="margin:30px">
                            <h4 class="label label-info" style="font-size:20px; color:#FFD200; background-color:#5B5B5F"><span class="glyphicon glyphicon-calendar" style="margin-right:10px; color:#FFD200"></span>Certificados a ayer</h4>
                            <div style="margin:40px">
                                <h4 style="margin-top:20px">Seleccione el tipo de certificado: </h4>
                                
                                <div class="btn-group btn-group-justified" style="margin-top:15px">
                                    <a onclick="showFechaSaldosContainer()" class="btn btn-default" style="font-size:18px">Saldos a la fecha<span class="glyphicon glyphicon-menu-down" style="margin-left:10px"></span></a>
                                    <a onclick="showFechaMovsContainer()" class="btn btn-default" style="font-size:18px">Movimientos a la fecha<span class="glyphicon glyphicon-menu-down" style="margin-left:10px"></span></a>
                                    <a onclick="showFechaSaldosMovsContainer()" class="btn btn-default" style="font-size:18px">Saldos + Movimientos a la fecha<span class="glyphicon glyphicon-menu-down" style="margin-left:10px"></span></a>
                                </div>

                                <div id="fechaSaldosContainer" class="animated fadeIn" style="display:none">
                                    <form action="certs/CertFechaSaldos.asp" method="post">
                                        <table>
                                            <tr>
                                                <h4 style="margin-top:15px; color:#5B5B5F">Certificado de saldos a la fecha<span class="glyphicon glyphicon-list-alt" style="margin-left:10px"></span></h4>
                                                <h4 style="margin-top:20px; color:#5B5B5F">Identificación<span class="glyphicon glyphicon-user" style="margin-left:10px"></span></h4>
                                                <tr><input type="text" required name="identificacion" class="form-control" onkeypress="return blockKeyboard(this, event, numbers);" placeholder="Ingrese la cédula o NIT del cliente"></tr>
                                            </tr>
                                            <tr>
                                                <button type="submit" class="btn" style="margin-top:15px; float:right; font-size:16px; background-color:#073D86; color:white"><span class="glyphicon glyphicon-search" style="margin-right:10px"></span>Consultar</button>
                                            </tr>
                                        </table>
                                    </form>
                                </div>

                                <div id="fechaMovsContainer" class="animated fadeIn" style="display:none">
                                    <form action="certs/CertFechaMovs.asp" method="post">
                                        <table>
                                            <tr>
                                                <h4 style="margin-top:15px; color:#5B5B5F">Certificado de movimientos a la fecha<span class="glyphicon glyphicon-list-alt" style="margin-left:10px"></span></h4>
                                                <h4 style="margin-top:20px; color:#5B5B5F">Identificación<span class="glyphicon glyphicon-user" style="margin-left:10px"></span></h4>
                                                <tr><input type="text" required name="identificacion" class="form-control" onkeypress="return blockKeyboard(this, event, numbers);" placeholder="Ingrese la cédula o NIT del cliente"></tr>
                                            </tr>
                                            <tr>
                                                <button type="submit" class="btn" style="margin-top:15px; float:right; font-size:16px; background-color:#073D86; color:white"><span class="glyphicon glyphicon-search" style="margin-right:10px"></span>Consultar</button>
                                            </tr>
                                        </table>
                                    </form>
                                </div>

                                <div id="fechaSaldosMovsContainer" class="animated fadeIn" style="display:none">
                                    <form action="certs/CertFechaSaldosMovs.asp" method="post">
                                        <table>
                                            <tr>
                                                <h4 style="margin-top:15px; color:#5B5B5F">Certificado de saldos con movimientos a la fecha<span class="glyphicon glyphicon-list-alt" style="margin-left:10px"></span></h4>
                                                <h4 style="margin-top:20px; color:#5B5B5F">Identificación<span class="glyphicon glyphicon-user" style="margin-left:10px"></span></h4>
                                                <tr><input type="text" required name="identificacion" class="form-control" onkeypress="return blockKeyboard(this, event, numbers);" placeholder="Ingrese la cédula o NIT del cliente"></tr>
                                            </tr>
                                            <tr>
                                                <button type="submit" class="btn" style="margin-top:15px; float:right; font-size:16px; background-color:#073D86; color:white"><span class="glyphicon glyphicon-search" style="margin-right:10px"></span>Consultar</button>
                                            </tr>
                                        </table>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="month" class="tab-pane">
                        <div class="animated fadeIn" style="margin:30px">
                            <h4 class="label label-info" style="font-size:20px; color:#FFD200; background-color:#5B5B5F"><span class="glyphicon glyphicon-calendar" style="margin-right:10px; color:#FFD200"></span>Certificados a mes específico</h4>
                            <div style="margin:40px">
                                <h4 style="margin-top:20px">Seleccione el tipo de certificado: </h4>

                                <div class="btn-group btn-group-justified" style="margin-top:15px">
                                    <a onclick="showMesSaldosContainer()" class="btn btn-default" style="font-size:18px">Saldos a mes específico<span class="glyphicon glyphicon-menu-down" style="margin-left:10px"></span></a>
                                </div>

                                <div id="mesSaldosContainer" class="animated fadeIn" style="display:none">
                                    <form action="certs/CertMesSaldos.asp" method="post">
                                        <table>
                                            <tr>
                                                <h4 style="margin-top:15px">Certificado de saldos a mes específico (año vigente)<span class="glyphicon glyphicon-list-alt" style="margin-left:10px"></span></h4>
                                                <h4 style="margin-top:20px">Identificación<span class="glyphicon glyphicon-user" style="margin-left:10px"></span></h4>
                                                <input type="text" required name="identificacion" class="form-control" onkeypress="return blockKeyboard(this, event, numbers);" placeholder="Ingrese la cédula o NIT del cliente">
                                            </tr>
                                            <tr>
                                                <h4>Mes<span class="glyphicon glyphicon-calendar" style="margin-left:10px"></span></h4>
                                                <select name="mes" style="width:100%; height:35px; border-radius:4px">
                                                    <option value="01">Enero</option>
                                                    <option value="02">Febrero</option>
                                                    <option value="03">Marzo</option>
                                                    <option value="04">Abril</option>
                                                    <option value="05">Mayo</option>
                                                    <option value="06">Junio</option>
                                                    <option value="07">Julio</option>
                                                    <option value="08">Agosto</option>
                                                    <option value="09">Septiembre</option>
                                                    <option value="10">Octubre</option>
                                                    <option value="11">Noviembre</option>
                                                    <option value="12">Diciembre</option>
                                                </select>
                                            </tr>
                                            <tr>
                                                <button type="submit" class="btn" style="margin-top:15px; float:right; font-size:16px; background-color:#073D86; color:white"><span class="glyphicon glyphicon-search" style="margin-right:10px"></span>Consultar</button>
                                            </tr>
                                        </table>
                                    </form>
                                </div>

                                <div id="mesMovsContainer" class="animated fadeIn" style="display:none">
                                    <form action="certs/CertMesMovs.asp" method="post">
                                        <table>
                                            <tr>
                                                <h4 style="margin-top:15px">Certificado de movimientos a mes específico (año vigente)<span class="glyphicon glyphicon-list-alt" style="margin-left:10px"></span></h4>
                                                <h4 style="margin-top:20px">Identificación <span class="glyphicon glyphicon-user"></span></h4>
                                                <input type="text" required name="identificacion" class="form-control" onkeypress="return blockKeyboard(this, event, numbers);" placeholder="Ingrese la cédula o NIT del cliente">
                                            </tr>
                                            <tr>
                                                <h4>Mes<span class="glyphicon glyphicon-calendar" style="margin-left:10px"></span></h4>
                                                <select name="mes" style="width:100%; height:35px; border-radius:4px">
                                                    <option value="01">Enero</option>
                                                    <option value="02">Febrero</option>
                                                    <option value="03">Marzo</option>
                                                    <option value="04">Abril</option>
                                                    <option value="05">Mayo</option>
                                                    <option value="06">Junio</option>
                                                    <option value="07">Julio</option>
                                                    <option value="08">Agosto</option>
                                                    <option value="09">Septiembre</option>
                                                    <option value="10">Octubre</option>
                                                    <option value="11">Noviembre</option>
                                                    <option value="12">Diciembre</option>
                                                </select>
                                            </tr>
                                            <tr>
                                                <button type="submit" class="btn" style="margin-top:15px; float:right; font-size:16px; background-color:#073D86; color:white"><span class="glyphicon glyphicon-search" style="margin-right:10px"></span>Consultar</button>
                                            </tr>
                                        </table>
                                    </form>
                                </div>

                                <div id="mesSaldosMovsContainer" class="animated fadeIn" style="display:none">
                                    <form action="certs/CertMesSalMovs.asp" method="post">
                                        <table>
                                            <tr>
                                                <h4 style="margin-top:15px">Certificados de saldos + movimientos a mes específico (año vigente)<span class="glyphicon glyphicon-list-alt" style="margin-left:10px"></span></h4>
                                                <h4 style="margin-top:20px">Identificación <span class="glyphicon glyphicon-user"></span></h4>
                                                <tr><input type="text" required name="identificacion" class="form-control" onkeypress="return blockKeyboard(this, event, numbers);" placeholder="Ingrese la cédula o NIT del cliente"></tr>
                                            </tr>
                                            <tr>
                                                <h4>Mes<span class="glyphicon glyphicon-calendar" style="margin-left:10px"></span></h4>
                                                <select name="mes" style="width:100%; height:35px; border-radius:4px">
                                                    <option value="01">Enero</option>
                                                    <option value="02">Febrero</option>
                                                    <option value="03">Marzo</option>
                                                    <option value="04">Abril</option>
                                                    <option value="05">Mayo</option>
                                                    <option value="06">Junio</option>
                                                    <option value="07">Julio</option>
                                                    <option value="08">Agosto</option>
                                                    <option value="09">Septiembre</option>
                                                    <option value="10">Octubre</option>
                                                    <option value="11">Noviembre</option>
                                                    <option value="12">Diciembre</option>
                                                </select>
                                            </tr>
                                            <tr>
                                                <button type="submit" class="btn" style="margin-top:15px; float:right; font-size:16px; background-color:#073D86; color:white"><span class="glyphicon glyphicon-search" style="margin-right:10px"></span>Consultar</button>
                                            </tr>
                                        </table>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="year" class="tab-pane">
                        <div class="animated fadeIn" style="margin:30px">
                            <h4 class="label label-info" style="font-size:20px; color:#FFD200; background-color:#5B5B5F"><span class="glyphicon glyphicon-calendar"style="margin-right:10px; color:#FFD200"></span>Certificados a año específico</h4>
                                <div style="margin:40px">
                                    <h4 style="margin-top:20px">Seleccione el tipo de certificado: </h4>
                                    <div class="btn-group btn-group-justified" style="margin-top:15px">
                                        <a onclick="showAnSaldosContainer()" class="btn btn-default" style="font-size:18px">Saldos a año específico<span class="glyphicon glyphicon-menu-down" style="margin-left:10px"></a>
                                    </div>

                                    <div id="anSaldosContainer" class="animated fadeIn" style="display:none">
                                        <form action="certs/CertAnSaldos.asp" method="post">
                                            <table>
                                                <tr>
                                                    <h4 style="margin-top:15px">Certificado de saldos a año específico<span class="glyphicon glyphicon-list-alt" style="margin-left:10px"></span></h4>
                                                    <h4 style="margin-top:20px">Identificación<span class="glyphicon glyphicon-user" style="margin-left:10px"></span></h4>
                                                    <input type="text" required required name="identificacion" class="form-control" onkeypress="return blockKeyboard(this, event, numbers);" placeholder="Ingrese la cédula o NIT del cliente">
                                                </tr>
                                                <tr>
                                                    <h4>Año<span class="glyphicon glyphicon-calendar" style="margin-left:10px"></span></h4>
                                                    <select name="an" style="width:100%; height:35px; border-radius:4px">
                                                        <option value="2014">2014</option>
                                                        <option value="2015">2015</option>
                                                        <option value="2016">2016</option>
                                                        <option value="2017">2017</option>
                                                        <option value="2018">2018</option>
                                                    </select>
                                                </tr>
                                                <tr>
                                                    <button type="submit" class="btn" style="margin-top:15px; float:right; font-size:16px; background-color:#073D86; color:white"><span class="glyphicon glyphicon-search" style="margin-right:10px"></span>Consultar</button>
                                                </tr>
                                            </table>
                                        </form>
                                    </div>

                                    <div id="anMovsContainer" class="animated fadeIn" style="display:none">
                                        <form action="certs/CertAnMovs.asp" method="post">
                                            <table>
                                                <tr>
                                                    <h4 style="margin-top:15px">Certificado de movimientos a año específico<span class="glyphicon glyphicon-list-alt" style="margin-left:10px"></span></h4>
                                                    <h4 style="margin-top:20px">Identificación<span class="glyphicon glyphicon-user" style="margin-left:10px"></span></h4>
                                                    <input type="text" required name="identificacion" class="form-control" onkeypress="return blockKeyboard(this, event, numbers);" placeholder="Ingrese la cédula o NIT del cliente">
                                                </tr>
                                                <tr>
                                                    <h4>Año<span class="glyphicon glyphicon-calendar" style="margin-left:10px"></span></h4>
                                                    <select name="an" style="width:100%; height:35px; border-radius:4px">
                                                        <option value="2014">2014</option>
                                                        <option value="2015">2015</option>
                                                        <option value="2016">2016</option>
                                                        <option value="2017">2017</option>
                                                        <option value="2018">2018</option>
                                                    </select>
                                                </tr>
                                                <tr>
                                                    <button type="submit" class="btn" style="margin-top:15px; float:right; font-size:16px; background-color:#073D86; color:white"><span class="glyphicon glyphicon-search" style="margin-right:10px"></span>Consultar</button>
                                                </tr>
                                            </table>
                                        </form>
                                    </div>

                                    <div id="anSaldosMovsContainer" class="animated fadeIn" style="display:none">
                                        <form action="certs/CertAnSalMovs.asp" method="post">
                                            <table>
                                                <tr>
                                                    <h4 style="margin-top:15px">Certificado de saldos + movimientos a año específico<span class="glyphicon glyphicon-list-alt" style="margin-left:10px"></span></h4>
                                                    <h4 style="margin-top:20px">Identificación<span class="glyphicon glyphicon-user" style="margin-left:10px"></span></h4>
                                                    <input type="text" required name="identificacion" class="form-control" onkeypress="return blockKeyboard(this, event, numbers);" placeholder="Ingrese la cédula o NIT del cliente">
                                                </tr>
                                                <tr>
                                                    <h4>Año<span class="glyphicon glyphicon-calendar" style="margin-left:10px"></span></h4>
                                                    <select name="an" style="width:100%; height:35px; border-radius:4px">
                                                        <option value="2014">2014</option>
                                                        <option value="2015">2015</option>
                                                        <option value="2016">2016</option>
                                                        <option value="2017">2017</option>
                                                        <option value="2018">2018</option>
                                                    </select>
                                                </tr>
                                                <tr>
                                                    <button type="submit" class="btn" style="margin-top:15px; float:right; font-size:16px; background-color:#073D86; color:white"><span class="glyphicon glyphicon-search" style="margin-right:10px"></span>Consultar</button>
                                                </tr>
                                            </table>
                                        </form>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>    

            </div>
        </div> 

        </div>   
    <% end if %>    

    <% if perfil = "0" then %>
        <script language="Javascript">
            $(document).ready(function(){
                $("#showMod").modal("show");
            });
        </script>
        <div class="modal fade" id="showMod" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <img src="img/logo.png" style="float:left; width:140px; height:35px">
                    </div>
                    <div class="modal-body">
                        <h3 class="modal-title">Revise sus credenciales de acceso<span class="glyphicon glyphicon-remove" style="margin-left:10px"></span></h3>
                        <h4>No dispone de los permisos administrativos para ingresar al portal.</h4>
                        <h4>Por favor contacte a su auxiliar administrativo para solicitar el acceso.</h4>
                    </div>
                    <div class="modal-footer">
                        <a style="text-decoration:none; font-size:20px; color:black" href="mailto:sebastian.castanomo@upb.edu.co?subject=Solicitud de acceso a plataforma de generación de certificados de deuda&body=DATOS DE INSCRIPCIÓN A PORTAL DE GENERACIÓN DE CERTIFICADOS DE DEUDA %0D%0A %0D%0ANOMBRES Y APELLIDOS COMPLETOS: %0D%0ADOCUMENTO DE IDENTIDAD: %0D%0AAREA: %0D%0AUSUARIO DE RED BANCOLOMBIA: ">Solicitar acceso<span class="glyphicon glyphicon-envelope" style="margin-left:10px"></span></a>
                    </div>
                </div>
            </div>
        </div>
        
    <% end if %>
    <%
        rs.Close
		cnx.Close
    %>
</body>
</html>