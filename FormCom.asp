<html>
<head>
	<meta charset="UTF-8">
	<link rel="icon" href="img/icon.ico">
    <link rel="stylesheet" type="text/css" href="css/animate.css"> 
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">  
	<script type="text/javascript" src="js/jquery-2.1.3.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script> 
	<script type="text/javascript" src="js/KeyboardValidation.js"></script>
    <title>Actualización de datos</title>
</head>
<body>
    <%
		perfil = "0"
        user = lcase(request.serverVariables("AUTH_USER"))
		identificacion = Request.form("identificacion")	

		Set cnx = Server.CreateObject("ADODB.Connection")
		cnx.Open "Provider=Microsoft.Jet.OLEDB.4.0; DATA SOURCE=" & server.mappath("./certs/DB.mdb")

		Set rs = Server.CreateObject("ADODB.RecordSet")
		Set rsUser = server.createObject("ADODB.RecordSet")
		rs.Open "SELECT * FROM tbl_users WHERE Documento="& identificacion &" AND Perfil='Comercial';", cnx
		rsUser.open "SELECT Nombre, Perfil FROM tbl_users WHERE UsuarioBC='"& user &"'", cnx

		if rsUser.EOF = false then
            perfil = rsUser.fields(1).value          
		end if
	%>
	<% if rs.EOF = true then %>
		<script language="Javascript">
            $(document).ready(function(){
                $("#showMod").modal("show");
            });
        </script>
        <div class="modal fade" id="showMod" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <img src="img/logo.png" style="float:left; width:140px; height:35px">
                    </div>
                    <div class="modal-body">
                        <h3 class="modal-title">Usuario no encontrado<span class="glyphicon glyphicon-user" style="margin-left:10px"></span></h3>
                        El número de identificación que ha ingresado no se encuentra, por favor verifique la información ingresada. 
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn" onclick="window.location.href='Index.asp'" style="background-color:#073D86; color:white" data-dismiss="modal">Ir a inicio</button>
                    </div>
                </div>
            </div>
        </div>
	<% end if %>
		
	<% 	
		Matriz = rs.Getrows	
		rs.Close
	%>
	<% if perfil = "Administrativo" then %>
		<div style="margin:55px" class="animated fadeIn">
			<nav class="navbar navbar-inverse navbar-fixed-top" style="background-color:#073D86">
				<div class="container-fluid">
					<div class="navbar-header">
						<img src="img/logo-banner.png" style="width:200px; height:50px; margin-top:15px; margin-bottom:10px; margin-left:15px;">
					</div>
					<div id="navbar" class="navbar-collapse collapse">
						<div class="nav navbar-nav navbar-right">              
							<h4 class="label" style="color:#073D86; font-size:18px; background-color:#FFD302; margin:15px; padding-top:10px"><span class="glyphicon glyphicon-user" style="font-size:25px; margin-right:10px; margin-top:20px" aria-hidden="true"></span>Usuario: <%=Response.write(rsUser.fields(0).value)%> Rol: <%=Response.write(rsUser.fields(1).value)%></h4>
						</div>
					</div>
				</div>
			</nav>
			<div style="float:right">
				<a href="./Index.asp" style="margin-left:180px; text-decoration:none; font-size:25px; color:#073D86">Volver a inicio<span class="glyphicon glyphicon-home" style="margin-left:10px"></span></a>
			</div> 
			<h2 style="margin-top:110px">Actualización de datos de asesor comercial</h2>
			<h4>Actualice los campos que requiera:</h4>
			<div class="panel panel-default">
				<div class="panel-body">
					<h4 class="label" style="font-size:20px; color:#073D86"><span class="glyphicon glyphicon-pencil" style="margin-right:10px; color:#073D86"></span>Actualizar asesor comercial</h4>
					<form action="UpdateCom.asp?id=<%=identificacion%>" method="post">
						<table>
							<tr>
								<h4>Nombres y apellidos</h4>
								<input type="text" name="nombre" value="<%=Matriz(0,0)%>" onkeypress="return blockKeyboard(this, event, keys);" class="form-control">
							</tr>

							<tr>
								<h4>Area</h4>
								<input type="text" name="area" value="<%=Matriz(2,0)%>" onkeypress="return blockKeyboard(this, event, keys);" class="form-control">
							</tr>

							<tr>
								<h4>Perfil</h4>
								<input type="text" name="perfil" value="<%=Matriz(3,0)%>" disabled class="form-control">
							</tr>

							<tr>
								<h4>Usuario Bancolombia</h4>
								<input type="text" name="user" value="<%=Matriz(4,0)%>" onkeypress="return blockKeyboard(this, event, keys);" class="form-control">
							</tr>
							<tr>
								<button type="submit" class="btn" style="margin-top:15px; float:right; background-color:#073D86; color:white"><span class="glyphicon glyphicon-ok" style="margin-right:10px"></span>Actualizar usuario</button></tr>
							</tr>
						</table>
					</form>
				</div>
			</div>
			
		</div>
	<% end if %>
	<% if perfil = "0" then %>
        <script language="Javascript">
            $(document).ready(function(){
                $("#showMod").modal("show");
            });
        </script>
        <div class="modal fade" id="showMod" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <img src="img/logo.png" style="float:left; width:140px; height:35px">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h3 class="modal-title">Revise sus credenciales de acceso<span class="glyphicon glyphicon-remove" style="margin-left:10px"></span></h3>
                        <h4>No dispone de los permisos administrativos para ingresar al portal.</h4>
                        <h4>Por favor contacte a su auxiliar administrativo para solicitar el acceso.</h4>
                    </div>
                    <div class="modal-footer">
                        <a style="text-decoration:none; font-size:20px; color:black" href="mailto:sebastian.castanomo@upb.edu.co?subject=Solicitud de acceso a plataforma de generación de certificados de deuda&body=DATOS DE INSCRIPCIÓN A PORTAL DE GENERACIÓN DE CERTIFICADOS DE DEUDA %0D%0A %0D%0ANOMBRES Y APELLIDOS COMPLETOS: %0D%0ADOCUMENTO DE IDENTIDAD: %0D%0AAREA: %0D%0AUSUARIO DE RED BANCOLOMBIA: ">Solicitar acceso<span class="glyphicon glyphicon-envelope" style="margin-left:10px"></span></a>
                    </div>
                </div>
            </div>
        </div>
    <% end if %>
	<%
		rsUser.Close
		cnx.Close
	%>
	</body>
</html>